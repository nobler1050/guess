#!/bin/python
""" Guessing game """

from random import randint

def guess_game():
    """ input user guess and output instructions """
    random = randint(1, 100)
    print "Guess a number between 1 and 100"
    guess = []
    while True:
        guess.append(raw_input())
        try:
            if int(guess[-1]) < random:
                print "Higher"
            elif int(guess[-1]) > random:
                print "Lower"
            elif int(guess[-1]) == random:
                print "Correct! It took you %d tries to guess the number" % len(guess)
                break
        except ValueError:
            if guess[-1] == "t":
                print "The number was %d" % random
                break
            elif guess[-1] == "q":
                break

guess_game()
