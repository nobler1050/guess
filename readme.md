Excercise
=========

>Create a python program called 'guess.py' that selects a random number from 1-100 (inclusive) and asks the user to guess the number. The user hits enter after the number. If the users guess is too low, then the program will respond with a single line that says "Higher.". If the users guess is too high then the program responds with "Lower." When the user guesses the correct number, then the program responds with "Correct! It took you x tries to guess the number" where x is the number of tries. If the user enters a 't' then the program should tell the user the number and exit the program. If the user enters 'q' then the program should immediately end.
Your porgram should be able to run from the command line, using 'python yourprogram.py

Programs due Thursday 2/25, at noon'
